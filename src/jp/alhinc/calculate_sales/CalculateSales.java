package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_SERIAL_NUMBER_ERROR = "売上ファイル名が連番になっていません。";
	private static final String SALE_AMOUNT_ERROR = "合計金額が10桁を超えました。";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されていなかった場合のエラー処理
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		//商品コードと商品名を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//branch.lstを読み込む際の正規表現の変数
		String branchRegex = "^[0-9]{3}";

		//commodity.lstを読み込む際の正規表現の変数
		String commodityRegex = "^[A-Za-z0-9]{8}";

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchRegex, "支店")) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityRegex, "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//売上集計課題フォルダの中のファイルを配列filesに代入
		File[] files = new File(args[0]).listFiles();

		//rcdファイルを保持するためのList
		ArrayList<File> rcdFiles = new ArrayList<>();

		//配列filesに代入したファイルをfile数の数だけ取得する
		for(int i = 0; i< files.length; i++) {

			String fileName = files[i].getName();

			//売上情報がファイル形式かつrcdファイルのものを条件分岐で取得
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//rcdファイル名が連番でなかった場合のエラー処理
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if(latter - former != 1) {
				System.out.println(FILE_SERIAL_NUMBER_ERROR);
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i++) {

			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;

				//売上ファイルの中身を保持するためのリスト
				ArrayList<String> saleFiles = new ArrayList<>();
				while((line = br.readLine()) != null) {

					saleFiles.add(line);
				}
				//売上ファイルが3行ではなかった時のエラー処理
				if(saleFiles.size() != 3) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です。");
					return;
				}
				//売上ファイルの支店コードが支店定義ファイルになかった時のエラー処理
				if(!branchNames.containsKey(saleFiles.get(0))) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です。");
					return;
				}
				//売上ファイルの商品コードが商品定義ファイルになかったときのエラー処理
				if(!commodityNames.containsKey(saleFiles.get(1))) {
					System.out.println(rcdFiles.get(i) + "の商品コードが不正です。");
				}
				//売上ファイルの売上金額が数字でなかった場合のエラー処理
				if(!saleFiles.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上金額をLongに変換
				Long fileSale = Long.parseLong(saleFiles.get(2));

				//branchSalesの初期売上金額の０円とfileSaleを足す
				Long branchSaleAmount = branchSales.get(saleFiles.get(0)) + fileSale;

				//commoditySalesの初期売上金額の0円とfileSaleを足す
				Long commoditySaleAmount = commoditySales.get(saleFiles.get(1)) + fileSale;

				//売上金額の合計が10桁超えた時のエラー処理
				if(branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(SALE_AMOUNT_ERROR);
					return;
				}

				//branchSalesに売上金額を格納
				branchSales.put(saleFiles.get(0), branchSaleAmount);

				//commoditySalesに売上金額を格納
				commoditySales.put(saleFiles.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
			} finally {
				if(br != null) {
					try{
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0],  FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regex, String errorType) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルが存在しなかった場合のエラー処理
			if(!file.exists()) {
				System.out.println(errorType + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//支店定義ファイル、商品定義ファイルのフォーマットが不正だった時のエラー処理
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(errorType + INVALID_FORMAT);
					return false;
				}

				sales.put(items[0], 0L);
				names.put(items[0], items[1]);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
		} finally {
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
